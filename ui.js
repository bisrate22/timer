let countdownElement = document.querySelector(".timer");
let startButton = document.querySelector("#start");
let stopButton = document.querySelector("#stop");

let startingMinute = 1;
let time = startingMinute * 60;

function updateTimer() {
  const minutes = Math.floor(time / 60);
  let seconds = time % 60;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  countdownElement.innerHTML = `${minutes} : ${seconds}`;
  time--;
}

startButton.addEventListener("click", () => {
  setInterval(updateTimer, 1000);
});

stopButton.addEventListener("click", () => {
  clearInterval();
});
