let countdownElement = document.querySelector(".timer"),
  startButton = document.querySelector("#start"),
  stopButton = document.querySelector("#stop");

let minutes, seconds;

let myInterval;

class TimerCounter {
  constructor(startingMinute) {
    this.startingMinute = startingMinute;
    this.time = startingMinute * 60;
  }
  updateTimer() {
    minutes = Math.floor(this.time / 60);
    seconds = this.time % 60;
    console.log(minutes);
    console.log(seconds);

    seconds = seconds < 10 ? "0" + seconds : seconds;

    countdownElement.innerHTML = `${minutes}:${seconds}`;

    this.time--;
    // console.log(this.time--);

    // return {
    //   minutes,
    //   seconds,
    // };
  }
  stopTimer() {
    console.log("stop");
    return clearInterval(myInterval);
  }
}

const timerCounter = new TimerCounter(10);

startButton.addEventListener("click", () => {
  console.log(timerCounter.updateTimer());

  // call updatetimer on a second interval
  // setInterval(timerCounter.updateTimer, 1000);
});
stopButton.addEventListener("click", timerCounter.stopTimer);
